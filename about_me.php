<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Begin Head -->

<!-- Mirrored from www.codesymbol.com/templates/uno/dark/page_about_me.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 24 Sep 2016 13:38:08 GMT -->
<head>

    <meta charset="utf-8">
    <title>Uno Photography</title>

    <!-- Begin Meta Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="A Photography Template by CodeSymbol"/>
    <meta name="keywords" content="HTML, CSS, JavaScript, Responsive, Photography"/>
    <meta name="author" content="CodeSymbol"/>
    <!-- End Meta Tags -->

    <link rel="icon" href="images/favicon.ico" />

    <!-- Begin Stylesheets -->
    <link type="text/css" rel="stylesheet" href="css/reset.css">
    <link type="text/css" rel="stylesheet" href="includes/entypo/style.css">
    <link type="text/css" rel="stylesheet" href="includes/icomoon/style.css">
    <link type="text/css" rel="stylesheet" href="includes/font_awesome/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="includes/cosy/style.css">
    <link type="text/css" rel="stylesheet" href="js/jquery-ui/jquery-ui-1.10.3.custom.min.css">
    <link type="text/css" rel="stylesheet" href="js/flexslider/style.css">
    <link type="text/css" rel="stylesheet" href="js/Magnific-Popup/magnific-popup.css">
    <link type="text/css" rel="stylesheet" href="js/mb.YTPlayer/css/YTPlayer.css">
    <link type="text/css" rel="stylesheet" href="css/animate.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">
    <!-- End Stylesheets -->


</head>
<!-- End Head -->


<!-- Begin Body -->
<body>

    <!-- Begin Loader -->
    <div class="loader" data-background-color="#000000" data-text-color="#ffffff">
        <p>LOADING</p>
        <span class="circle"></span>
    </div>
    <!-- End Loader -->

    <!-- Begin Header -->
    <?php 
        include("includes/nav.php");
    ?>
    <!-- End Header -->


    <!-- Begin Content -->
    <div class="main-container" style="background-image:url(images/pages/p2_1.png)">

        <div class="min-style">
            <div class="content-wrapper clearfix">

                <div class="row">

                    <div class="col three-fourth">

                        <div class="info-box col one-fourth">
                            <p class="desc">NAME</p>
                            <p class="info">TAYA WILLIAMS</p>
                        </div>
                        <div class="info-box col one-fourth">
                            <p class="desc">PLACE</p>
                            <p class="info">PARIS, FRANCE</p>
                        </div>
                        <div class="info-box col one-fourth">
                            <p class="desc">WORK</p>
                            <p class="info">FREELANCER</p>
                        </div>
                        <div class="info-box col one-fourth">
                            <p class="desc">TITLE</p>
                            <p class="info">PHOTOGRAPHER</p>
                        </div>

                        <div class="row clearfix">
                            <div class="col full" style="color:#ffffff;">

                                <p>Vestibulum tellus risus, pretium et facilisis nec, porta in felis. Nullam fermentum, lorem nec tincidunt tempus, lectus venenatis nisi, quis ultrices tortor arcu id diam. Nunc eros est.</p>

                                <p>Vestibulum tellus risus, pretium et facilisis nec, porta in felis. Nullam fermentum, lorem nec tincidunt tempus.</p>

                                <div class="divider" style="height:20px;"></div>
                                
                            </div>
                        </div>

                        <div class="row">
                            <div class="col full">
                                
                                <div class="phone"><p>+51 (0)6 21 51 43 88</p></div>

                                <div class="social-icons">

                                    <a href="#" class="icon1-facebook"></a>
                                    <a href="#" class="icon1-twitter"></a>
                                    <a href="#" class="icon3-pinterest"></a>
                                    <a href="#" class="icon1-google-plus"></a>

                                </div>
                                
                            </div>
                        </div>

                    </div>
                    
                    <img class="personal-img" src="images/pages/p2_2.png" alt="">

                </div>

                <div class="clear"></div>

            </div>
        </div>

    </div>
    <!-- End Content -->


    <!-- Begin Footer -->
    <?php 
        include("includes/footer.php");
    ?>
    <!-- End Footer -->


    <!-- Begin JavaScript -->
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/modernizr-respond.js"></script>
    <script type="text/javascript" src="js/cookie.js"></script>
    <script type="text/javascript" src="js/retina.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/scrollTo-min.js"></script>
    <script type="text/javascript" src="js/easing.1.3.js"></script>
    <script type="text/javascript" src="js/appear.js"></script>
    <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/flexslider/flexslider.min.js"></script>
    <script type="text/javascript" src="js/isotope.min.js"></script>
    <script type="text/javascript" src="js/queryloader2.min.js"></script>
    <script type="text/javascript" src="js/gmap.min.js"></script>
    <script type="text/javascript" src="js/nicescroll.min.js"></script>
    <script type="text/javascript" src="js/fitvids.js"></script>
    <script type="text/javascript" src="js/Magnific-Popup/magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/mb.YTPlayer/inc/mb.YTPlayer.js"></script>
    <script type="text/javascript" src="js/mousewheel.min.js"></script>
    <script type="text/javascript" src="js/lazyload.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
    <!-- End JavaScript -->


</body>
<!-- End Body -->


<!-- Mirrored from www.codesymbol.com/templates/uno/dark/page_about_me.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 24 Sep 2016 13:38:34 GMT -->
</html>