<header>
    <a href="index.php" class="logo-container">
        <p>AASHITA SHAH PHOTOGRAPHY</p>
    </a>
    <nav>
        <ul>
            <li>
                <a href="index.php" class="active">HOME</a> 
            </li>
            <li>
                <a href="portfolio.php">
                     PORTFOLIO
                </a>
            </li>
            <li><a href="about_me.php">ABOUT ME</a></li>
            <li><a href="contact.php">CONTACT ME</a></li>
        </ul>
    </nav>
    <a href="#" class="mobile-navigation icon3-list2"></a>
</header>
