    <footer>

        <p class="copyrights">© AASHITA SHAH PHOTOGRAPHY | ALL RIGHTS RESERVED</p>

        <div class="social-networks clearfix">
            <a href="#">
                <span>FACEBOOK</span>
                <i class="facebook-icon icon1-facebook"></i>
            </a>
            <a href="#">
                <span>TWITTER</span>
                <i class="twitter-icon icon1-twitter"></i>
            </a>
            <a href="#">
                <span>PINTEREST</span>
                <i class="pinterest-icon icon3-pinterest"></i>
            </a>
            <a href="#">
                <span>INSTAGRAM</span>
                <i class="instagram-icon icon3-instagram"></i>
            </a>
        </div>

    </footer>