<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Begin Head -->

<!-- Mirrored from www.codesymbol.com/templates/uno/dark/portfolio_single_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 24 Sep 2016 13:37:27 GMT -->
<head>

    <meta charset="utf-8">
    <title>Uno Photography</title>

    <!-- Begin Meta Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="A Photography Template by CodeSymbol"/>
    <meta name="keywords" content="HTML, CSS, JavaScript, Responsive, Photography"/>
    <meta name="author" content="CodeSymbol"/>
    <!-- End Meta Tags -->

    <link rel="icon" href="images/favicon.ico" />

    <!-- Begin Stylesheets -->
    <link type="text/css" rel="stylesheet" href="css/reset.css">
    <link type="text/css" rel="stylesheet" href="includes/entypo/style.css">
    <link type="text/css" rel="stylesheet" href="includes/icomoon/style.css">
    <link type="text/css" rel="stylesheet" href="includes/font_awesome/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="includes/cosy/style.css">
    <link type="text/css" rel="stylesheet" href="js/jquery-ui/jquery-ui-1.10.3.custom.min.css">
    <link type="text/css" rel="stylesheet" href="js/flexslider/style.css">
    <link type="text/css" rel="stylesheet" href="js/Magnific-Popup/magnific-popup.css">
    <link type="text/css" rel="stylesheet" href="js/mb.YTPlayer/css/YTPlayer.css">
    <link type="text/css" rel="stylesheet" href="css/animate.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">
    <!-- End Stylesheets -->


</head>
<!-- End Head -->


<!-- Begin Body -->
<body>

    <!-- Begin Loader -->
    <div class="loader" data-background-color="#000000" data-text-color="#ffffff">
        <p>LOADING</p>
        <span class="circle"></span>
    </div>
    <!-- End Loader -->

    <!-- Begin Header -->
    <?php 
        include("includes/nav.php");
    ?>
    <!-- End Header -->


    <!-- Begin Content -->
    <div class="main-container with-padding">

        <!-- Begin Wrapper -->
        <div class="wrapper">

            <!-- Begin Portfolio -->
            <div class="portfolio-single style-1">

                <!-- Begin Navigation -->
                <div class="nav">
                    <a href="#" class="prev icon4-leftarrow23"></a>
                    <a href="#" class="next icon4-chevrons"></a>
                </div>
                <!-- End Navigation -->

                <!-- Begin Inner Wrapper -->
                <div class="row inner-wrapper">

                    <div class="col full content clearfix">

                        <div class="info">
                            <div class="inner">

                                <div class="info-box">
                                    <p class="desc">TITLE</p>
                                    <p class="info">CITY OF LOVE</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">CLIENT</p>
                                    <p class="info">Fusion Models NYC</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">PHOTOGRAPHER</p>
                                    <p class="info">ANDREW WILLSON</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">CAMERA</p>
                                    <p class="info">CANON EOS 6D</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">CATEGORY</p>
                                    <p class="info">MODELS, INDOORS</p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">WEBSITE</p>
                                    <p class="info"><a href="#">www.company.com</a></p>
                                </div>

                                <div class="info-box">
                                    <p class="desc">SHARE</p>
                                    <p class="info">
                                        <a href="#">FACEBOOK</a><br/>
                                        <a href="#">TWITTER</a><br/>
                                        <a href="#">GOOGLE+</a><br/>
                                        <a href="#">PINTEREST</a>
                                    </p>
                                </div>

                            </div>
                        </div>

                        <div class="images">

                            <a href="images/gallery/g1_1.html" rel="gallery">
                                <img class="lazy" data-original="images/gallery/g5_1.jpg" alt="" data-width="840" data-height="1260">
                            </a>
                            <a href="images/gallery/g1_2.html" rel="gallery">
                                <img class="lazy" data-original="images/gallery/g5_2.jpg" alt="" data-width="840" data-height="1260">
                            </a>
                            <a href="images/gallery/g1_3.html" rel="gallery">
                                <img class="lazy" data-original="images/gallery/g5_3.jpg" alt="" data-width="2048" data-height="1367">
                            </a>
                            <a href="images/gallery/g5_4.jpg" rel="gallery">
                                <img class="lazy" data-original="images/gallery/g5_4.jpg" alt="" data-width="2048" data-height="1356">
                            </a>
                            <a href="images/gallery/g5_5.jpg" rel="gallery">
                                <img class="lazy" data-original="images/gallery/g5_5.jpg" alt="" data-width="584" data-height="1024">
                            </a>
                            <a href="images/gallery/g5_6.jpg" rel="gallery">
                                <img class="lazy" data-original="images/gallery/g5_6.jpg" alt="" data-width="2048" data-height="1362">
                            </a>

                        </div>

                    </div>

                </div>
                <!-- End Inner Wrapper -->

            </div>
            <!-- End Portfolio -->

        </div>
        <!-- End Wrapper -->

    </div>
    <!-- End Content -->


    <!-- Begin Footer -->
    <?php 
        include("includes/footer.php");
    ?>
    <!-- End Footer -->


    <!-- Begin JavaScript -->
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/modernizr-respond.js"></script>
    <script type="text/javascript" src="js/cookie.js"></script>
    <script type="text/javascript" src="js/retina.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/scrollTo-min.js"></script>
    <script type="text/javascript" src="js/easing.1.3.js"></script>
    <script type="text/javascript" src="js/appear.js"></script>
    <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/flexslider/flexslider.min.js"></script>
    <script type="text/javascript" src="js/isotope.min.js"></script>
    <script type="text/javascript" src="js/queryloader2.min.js"></script>
    <script type="text/javascript" src="js/gmap.min.js"></script>
    <script type="text/javascript" src="js/nicescroll.min.js"></script>
    <script type="text/javascript" src="js/fitvids.js"></script>
    <script type="text/javascript" src="js/Magnific-Popup/magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/mb.YTPlayer/inc/mb.YTPlayer.js"></script>
    <script type="text/javascript" src="js/mousewheel.min.js"></script>
    <script type="text/javascript" src="js/lazyload.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
    <!-- End JavaScript -->


</body>
<!-- End Body -->


<!-- Mirrored from www.codesymbol.com/templates/uno/dark/portfolio_single_1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 24 Sep 2016 13:37:29 GMT -->
</html>