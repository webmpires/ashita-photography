<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Begin Head -->

<!-- Mirrored from www.codesymbol.com/templates/uno/dark/page_contact_2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 24 Sep 2016 13:39:12 GMT -->
<head>

    <meta charset="utf-8">
    <title>Uno Photography</title>

    <!-- Begin Meta Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="A Photography Template by CodeSymbol"/>
    <meta name="keywords" content="HTML, CSS, JavaScript, Responsive, Photography"/>
    <meta name="author" content="CodeSymbol"/>
    <!-- End Meta Tags -->

    <link rel="icon" href="images/favicon.ico" />

    <!-- Begin Stylesheets -->
    <link type="text/css" rel="stylesheet" href="css/reset.css">
    <link type="text/css" rel="stylesheet" href="includes/entypo/style.css">
    <link type="text/css" rel="stylesheet" href="includes/icomoon/style.css">
    <link type="text/css" rel="stylesheet" href="includes/font_awesome/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="includes/cosy/style.css">
    <link type="text/css" rel="stylesheet" href="js/jquery-ui/jquery-ui-1.10.3.custom.min.css">
    <link type="text/css" rel="stylesheet" href="js/flexslider/style.css">
    <link type="text/css" rel="stylesheet" href="js/Magnific-Popup/magnific-popup.css">
    <link type="text/css" rel="stylesheet" href="js/mb.YTPlayer/css/YTPlayer.css">
    <link type="text/css" rel="stylesheet" href="css/animate.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css">
    <!-- End Stylesheets -->


</head>
<!-- End Head -->


<!-- Begin Body -->
<body>

    <!-- Begin Loader -->
    <div class="loader" data-background-color="#000000" data-text-color="#ffffff">
        <p>LOADING</p>
        <span class="circle"></span>
    </div>
    <!-- End Loader -->

    <!-- Begin Header -->
    <?php 
        include("includes/nav.php");
    ?>
    <!-- End Header -->


    <!-- Begin Content -->
    <div class="main-container with-padding">

        <div class="wrapper">

            <div class="inner-wrapper">

                <div class="contact-2">

                    <div class="content-inner clearfix">

                        <div class="row">
                            <div class="col full">
                                <h4>CONTACT US</h4>
                            </div>
                        </div>

                        <div class="divider clear" style="height:10px;"></div>

                        <div class="row">

                            <div class="info-box col one-third">
                                <p class="desc">PHONE</p>
                                <p class="info">+51 (0)6 21 51 43 88</p>
                            </div>
                            <div class="info-box col one-third">
                                <p class="desc">ADDRESS</p>
                                <p class="info">30th Castle St Manhattan, NY</p>
                            </div>
                            <div class="info-box col one-third">
                                <p class="desc">EMAIL</p>
                                <p class="info">CONTACT@COMPANY.COM</p>
                            </div>

                        </div>

                        <div class="divider clear" style="height:20px;"></div>

                        <div class="row">

                            <div class="two-third col">

                                <form method="post" action="http://www.codesymbol.com/templates/uno/dark/includes/contact_form.php">
                                    <input type="text" name="user_name" placeholder="NAME">
                                    <input type="email" name="user_email" placeholder="EMAIL">
                                    <input type="text" name="message_subject" placeholder="SUBJECT">
                                    <textarea name="message_content" placeholder="MESSAGE"></textarea>
                                    <input type="button" name="submit" value="SUBMIT">
                                    <p class="message-info">Message Info.</p>
                                </form>

                            </div>
                            <div class="one-third col">

                                <p>Vestibulum tellus risus, pretium et facilisis nec, porta in felis. Nullm fermentum, lorem nec tincidunt tempus, lectus venenatis nisi, quis ultrices tortor arcu .</p>

                                <p>felis. Nullm tellus risus, pretium fermentum, lorem nec tincidunt tempus, lectus venenatis nisi, quis.</p>

                                <div class="divider clear" style="height:10px;"></div>

                                <div class="info-box">
                                    <p class="desc">WORKING HOURS</p>
                                    <p class="info">MONDAY: 9 AM - 6 PM<br/>TUESDAY: 9 AM - 6 PM<br/>WEDNESDAY: 9 AM - 8 PM<br/>THURSDAY: 9 AM - 8 PM<br/>FRIDAY: 10 AM - 10 PM</p>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="map" data-address="London, United Kingdom">
                        <div class="open"><p>OPEN MAP</p></div>
                        <div class="map-content"></div>
                    </div>

                </div>

            </div>

        </div>


    </div>
    <!-- End Content -->


    <!-- Begin Footer -->
    <?php 
        include("includes/footer.php");
    ?>
    <!-- End Footer -->


    <!-- Begin JavaScript -->
    <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false'></script>
    <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="js/migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/modernizr-respond.js"></script>
    <script type="text/javascript" src="js/cookie.js"></script>
    <script type="text/javascript" src="js/retina.js"></script>
    <script type="text/javascript" src="js/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/scrollTo-min.js"></script>
    <script type="text/javascript" src="js/easing.1.3.js"></script>
    <script type="text/javascript" src="js/appear.js"></script>
    <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="js/flexslider/flexslider.min.js"></script>
    <script type="text/javascript" src="js/isotope.min.js"></script>
    <script type="text/javascript" src="js/queryloader2.min.js"></script>
    <script type="text/javascript" src="js/gmap.min.js"></script>
    <script type="text/javascript" src="js/nicescroll.min.js"></script>
    <script type="text/javascript" src="js/fitvids.js"></script>
    <script type="text/javascript" src="js/Magnific-Popup/magnific-popup.min.js"></script>
    <script type="text/javascript" src="js/mb.YTPlayer/inc/mb.YTPlayer.js"></script>
    <script type="text/javascript" src="js/mousewheel.min.js"></script>
    <script type="text/javascript" src="js/lazyload.min.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
    <!-- End JavaScript -->


</body>
<!-- End Body -->


<!-- Mirrored from www.codesymbol.com/templates/uno/dark/page_contact_2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 24 Sep 2016 13:39:12 GMT -->
</html>